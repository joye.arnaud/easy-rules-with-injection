package com.example.demo.rules.definitions;

import com.example.demo.entities.Person;
import org.jeasy.rules.api.Facts;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Sale rule test")
class SaleRuleTest {
    private SaleRule saleRule;

    private Person adult;
    private Person young;

    private Date date;

    @BeforeEach()
    public void beforeAll() {
        saleRule = new SaleRule();

        adult = new Person("testAdult", 25);
        young = new Person("testYoung", 1);

        date = new Date();
    }
    @Nested
    @DisplayName("Condition")
    class Condition {
        @Test
        void when_isAdult() {
            Facts facts = new Facts();
            facts.put("person", adult);
            facts.put("date", date);
            facts.put("isAdult", true);

            Boolean isAdultFact = facts.get("isAdult");

            assertTrue(saleRule.when(isAdultFact));
        }
        @Test()
        void when_isYoung() {
            Facts facts = new Facts();
            facts.put("person", young);
            facts.put("date", date);

            Boolean isAdultFact = facts.get("isAdult");

            assertThrows(NullPointerException.class, () -> {
                saleRule.when(isAdultFact);
            });
        }
    }
}