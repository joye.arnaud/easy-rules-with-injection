package com.example.demo.rules.definitions;

import com.example.demo.entities.Person;
import org.jeasy.rules.api.Facts;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Alcohol restriction rule test")
class AlcoholRestrictionRuleTest {
    private AlcoholRestrictionRule alcoholRestrictionRule;

    private Person adult;
    private Person young;

    private Date date;

    @BeforeEach()
    public void beforeAll() {
        alcoholRestrictionRule = new AlcoholRestrictionRule();

        adult = new Person("testAdult", 25);
        young = new Person("testYoung", 1);

        date = new Date();
    }
    @Nested
    @DisplayName("Condition")
    class Condition {
        @Test
        void when_isAdult() {
            Facts facts = new Facts();
            facts.put("person", adult);
            facts.put("date", date);
            facts.put("isAdult", true);

            assertFalse(alcoholRestrictionRule.when(facts));
        }
        @Test
        void when_isYoung() {
            Facts facts = new Facts();
            facts.put("person", young);
            facts.put("date", date);

            assertTrue(alcoholRestrictionRule.when(facts));
        }
    }
}