package com.example.demo.rules.definitions;

import com.example.demo.entities.Person;
import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.api.RulesEngine;
import org.jeasy.rules.core.DefaultRulesEngine;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Adult rule test")
class AdultRuleTest {
    private AdultRule adultRule;

    private Rules rules;
    private RulesEngine rulesEngine;

    private Person adult;
    private Person limit;
    private Person young;

    private Date date;

    @BeforeEach()
    public void beforeAll() {
        adultRule = new AdultRule();

        rules = new Rules();
        rulesEngine = new DefaultRulesEngine();
        rules.register(adultRule);

        adult = new Person("testAdult", 25);
        limit = new Person("testLimit", 18);
        young = new Person("testYoung", 1);

        date = new Date();
    }

    @Nested
    @DisplayName("Condition")
    class Condition {
        @Test
        void when() {
            assertFalse(adultRule.when(young, date));
            assertTrue(adultRule.when(limit, date));
            assertTrue(adultRule.when(adult, date));
        }
    }

    @Nested
    @DisplayName("Action")
    class Action {
        @Test
        void thenAct_adult() {
            Facts facts = new Facts();
            facts.put("person", adult);
            facts.put("date", date);

            assertNull(facts.getFact("isAdult"));

            rulesEngine.fire(rules, facts);

            assertNotNull(facts.getFact("isAdult"));
            assertTrue((Boolean) facts.getFact("isAdult").getValue());
        }

        @Test
        void thenAct_young() {
            Facts facts = new Facts();
            facts.put("person", young);
            facts.put("date", date);

            assertNull(facts.getFact("isAdult"));

            rulesEngine.fire(rules, facts);

            assertNull(facts.getFact("isAdult"));
        }
    }


}