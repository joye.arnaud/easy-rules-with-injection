package com.example.demo.rules;

import com.example.demo.rules.definitions.ARulesDefinition;
import com.example.demo.rules.definitions.AdultRule;
import com.example.demo.rules.definitions.AlcoholRestrictionRule;
import com.example.demo.rules.definitions.SaleRule;
import org.jeasy.rules.api.Rules;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Service
public class RulesManifestSpring {
    private final Map<ARulesDefinition, Boolean> ruleName;

    private final AdultRule adultRule;
    private final AlcoholRestrictionRule alcoholRestrictionRule;
    private final SaleRule saleRule;

    @Autowired
    RulesManifestSpring(AdultRule adultRule, AlcoholRestrictionRule alcoholRestrictionRule, SaleRule saleRule) {
        this.adultRule = adultRule;
        this.alcoholRestrictionRule = alcoholRestrictionRule;
        this.saleRule = saleRule;

        this.ruleName = new HashMap<>();
        ruleName.put(this.adultRule, true);
        ruleName.put(this.alcoholRestrictionRule, true);
        ruleName.put(this.saleRule, true);
    }

    private Set<ARulesDefinition> rulesDefinitions() {
        return this.ruleName.entrySet().stream()
                .filter(Map.Entry::getValue)
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());
    }

    public Rules getRules() {
        Rules rules = new Rules();

        rulesDefinitions().forEach(rules::register);

        return rules;
    }

}
