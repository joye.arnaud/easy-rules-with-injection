package com.example.demo.rules;

import com.example.demo.entities.Person;
import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.api.RulesEngine;
import org.jeasy.rules.core.DefaultRulesEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class RuleService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private RulesEngine rulesEngine;
    private Rules rules;

    @Autowired
    public RuleService(RulesEngine myEngine, RulesManifestSpring rulesManifestSpring) {
        rulesEngine = myEngine;
        rules = rulesManifestSpring.getRules();
    }

    private void printRules() {
        logger.info("==============================================");
        logger.info("Rules are :");
        rules.forEach(rule -> logger.info("===> {}", rule.getName()));
        logger.info("==============================================");
    }

    public void execute(List<Person> persons) {
        printRules();
        persons.forEach(person -> fireRulesOnPerson(rules, person));
    }

    private void fireRulesOnPerson(Rules rules, Person person) {
        Facts facts = new Facts();
        facts.put("person", person);
        facts.put("date", new Date());
        rulesEngine.fire(rules, facts);
    }

}
