package com.example.demo.rules.definitions;

import com.example.demo.entities.Person;
import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.annotation.Rule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Rule(
        name = "legal customer sale rule",
        description = "Discount applied to legal customer aka adult",
        priority = 2)
public class SaleRule extends ARulesDefinition {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    //FILTER ON FACTS WITHOUT isAdult KEY
    @Condition
    public boolean when(@Fact("isAdult") Boolean isAdult) {
        return isAdult;
    }

    @Action
    public void thenLog(@Fact("person") Person person) {
        logger.info("===> {} it's black friday, please see our special offer!", person.getName());
    }

}
