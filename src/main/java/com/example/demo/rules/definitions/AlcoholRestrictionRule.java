package com.example.demo.rules.definitions;

import com.example.demo.entities.Person;
import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.annotation.Rule;
import org.jeasy.rules.api.Facts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Rule(
        name = "alcohol restriction rule",
        description = "Check if person isn't allowed to drink alcohol",
        priority = 1)
public class AlcoholRestrictionRule extends ARulesDefinition {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    //KEEP ALL FACTS
    @Condition
    public boolean when(Facts facts) {
        return facts.get("isAdult") == null;
    }

    @Action
    public void thenLog(@Fact("person") Person person) {
        logger.info("===> {} is forbidden to drink", person.getName());
    }

}
