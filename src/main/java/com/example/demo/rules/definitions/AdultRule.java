package com.example.demo.rules.definitions;

import com.example.demo.entities.Person;
import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.annotation.Rule;
import org.jeasy.rules.api.Facts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Rule(
        name = "age rule",
        description = "Check if person's age is > 18 and marks the person as adult",
        priority = 0)
public class AdultRule extends ARulesDefinition {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Condition
    public boolean when(@Fact("person") Person person, @Fact("date") Date date) {
        return person.getAge() > 17;
    }

    @Action(order = 1)
    public void thenAct(Facts facts) {
        facts.add(new org.jeasy.rules.api.Fact<>("isAdult", true));
    }

    @Action(order = 2)
    public void thenLog(@Fact("person") Person person) {
        logger.info("===> {} is adult", person.getName());
    }

}
