package com.example.demo.rules.reporting;

import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rule;
import org.jeasy.rules.api.Rules;

import java.util.HashMap;
import java.util.Map;

public class Reporting {
    private Facts beforeEvaluateFact;
    private Facts afterExecuteFact;
    private Rules appliedRules;
    private Map<Rule, Boolean> conditionnalResult;

    public Reporting() {
        this.conditionnalResult = new HashMap<>();
    }

    public Reporting(Facts beforeEvaluateFact, Facts afterExecuteFact, Rules appliedRules, Map<Rule, Boolean> conditionnalResult) {
        this.beforeEvaluateFact = beforeEvaluateFact;
        this.afterExecuteFact = afterExecuteFact;
        this.appliedRules = appliedRules;
        this.conditionnalResult = conditionnalResult;
    }

    public Facts getBeforeEvaluateFact() {
        return beforeEvaluateFact;
    }

    public void setBeforeEvaluateFact(Facts beforeEvaluateFact) {
        this.beforeEvaluateFact = beforeEvaluateFact;
    }

    public Facts getAfterExecuteFact() {
        return afterExecuteFact;
    }

    public void setAfterExecuteFact(Facts afterExecuteFact) {
        this.afterExecuteFact = afterExecuteFact;
    }

    public Rules getAppliedRules() {
        return appliedRules;
    }

    public void setAppliedRules(Rules appliedRules) {
        this.appliedRules = appliedRules;
    }

    public Map<Rule, Boolean> getConditionnalResult() {
        return conditionnalResult;
    }

    public void setConditionnalResult(Map<Rule, Boolean> conditionnalResult) {
        this.conditionnalResult = conditionnalResult;
    }

    public void addConditionnalResult(Rule ruleName, Boolean conditonResult) {
        this.conditionnalResult.put(ruleName, conditonResult);
    }
}
