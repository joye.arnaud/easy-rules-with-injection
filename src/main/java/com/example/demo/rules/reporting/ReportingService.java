package com.example.demo.rules.reporting;

import com.example.demo.entities.Person;
import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rule;
import org.jeasy.rules.api.Rules;
import org.springframework.stereotype.Service;

@Service
public class ReportingService {
    private ReportingByPerson reportingByPerson;

    public ReportingService() {
        this.reportingByPerson = new ReportingByPerson();
    }

    public void newLine(Person person) {
        this.reportingByPerson.put(person, new Reporting());
    }

    public void setReportingBeforeFact(Person person, Facts fact) {
        final Reporting reporting = this.reportingByPerson.get(person);
        reporting.setBeforeEvaluateFact(fact);
    }

    public void setReportingAfterFact(Person person, Facts fact) {
        final Reporting reporting = this.reportingByPerson.get(person);
        reporting.setAfterExecuteFact(fact);
    }

    public void setAppliedRules(Person person, Rules appliedRules) {
        final Reporting reporting = this.reportingByPerson.get(person);
        reporting.setAppliedRules(appliedRules);
    }

    public void addNewConditionnalResult(Person person, Rule ruleName, Boolean conditionResult) {
        final Reporting reporting = this.reportingByPerson.get(person);
        reporting.addConditionnalResult(ruleName, conditionResult);
    }

    /*GETTER*/

    public ReportingByPerson getReportingByPerson() {
        return this.reportingByPerson;
    }
}
