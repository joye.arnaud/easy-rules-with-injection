package com.example.demo.rules.listener;

import com.example.demo.entities.Person;
import com.example.demo.rules.reporting.ReportingService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rule;
import org.jeasy.rules.api.RuleListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class MyRulesListener implements RuleListener {
    Log log = LogFactory.getLog(MyRulesListener.class);

    private final ReportingService reportingService;

    @Autowired
    private MyRulesListener(ReportingService reportingService) {
        this.reportingService = reportingService;
    }

    @Override
    public boolean beforeEvaluate(Rule rule, Facts facts) {
        return true;
    }

    @Override
    public void afterEvaluate(Rule rule, Facts facts, boolean b) {
        final Person person = facts.get("person");
        reportingService.addNewConditionnalResult(person, rule, b);
    }

    @Override
    public void beforeExecute(Rule rule, Facts facts) {
    }

    @Override
    public void onSuccess(Rule rule, Facts facts) {
    }

    @Override
    public void onFailure(Rule rule, Facts facts, Exception e) {
    }
}
