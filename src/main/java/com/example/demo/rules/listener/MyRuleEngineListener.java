package com.example.demo.rules.listener;

import com.example.demo.entities.Person;
import com.example.demo.rules.reporting.ReportingService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jeasy.rules.api.Facts;
import org.jeasy.rules.api.Rules;
import org.jeasy.rules.api.RulesEngineListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MyRuleEngineListener implements RulesEngineListener {
    Log log = LogFactory.getLog(MyRuleEngineListener.class);

    private final ReportingService reportingService;

    @Autowired
    private MyRuleEngineListener(ReportingService reportingService) {
        this.reportingService = reportingService;
    }

    @Override
    public void beforeEvaluate(Rules rules, Facts facts) {
        final Person person = facts.get("person");
        reportingService.newLine(person);
        reportingService.setReportingBeforeFact(person, facts);
    }

    @Override
    public void afterExecute(Rules rules, Facts facts) {
        final Person person = facts.get("person");
        reportingService.setReportingAfterFact(person, facts);
        reportingService.setAppliedRules(person, rules);
    }
}
