package com.example.demo.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RuleDefinitionException extends Exception {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public RuleDefinitionException(Exception error) {
        logger.error("Wrong rule declaration in manifest", error);
    }
}
