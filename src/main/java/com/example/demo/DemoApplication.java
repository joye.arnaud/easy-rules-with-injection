package com.example.demo;

import com.example.demo.entities.Person;
import com.example.demo.rules.reporting.ReportingByPerson;
import com.example.demo.rules.reporting.ReportingService;
import com.example.demo.rules.listener.MyRuleEngineListener;
import com.example.demo.rules.listener.MyRulesListener;
import com.example.demo.rules.RuleService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jeasy.rules.api.RulesEngine;
import org.jeasy.rules.core.DefaultRulesEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class DemoApplication {
    Log log = LogFactory.getLog(MyRulesListener.class);

    @Autowired
    private RuleService rules;

    @Autowired
    private ReportingService reportingService;

    @Bean(name = "myEngine")
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public RulesEngine myEngine(MyRulesListener myRulesListener, MyRuleEngineListener myRuleEngineListener) {
        log.info("create rule Engine");
        DefaultRulesEngine rulesEngine = new DefaultRulesEngine();
        rulesEngine.registerRuleListener(myRulesListener);
        rulesEngine.registerRulesEngineListener(myRuleEngineListener);
        return rulesEngine;
    }

    @Bean
    public CommandLineRunner commandLineRunner (ApplicationContext ctx) {
        return args -> {
            List<Person> persons = new ArrayList<>();
            persons.add(new Person("Tom", 20));
            persons.add(new Person("Sisi", 16));
            persons.add(new Person("Andy", 11));

            rules.execute(persons);

            final ReportingByPerson reportingByPerson = reportingService.getReportingByPerson();
        };
    }

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}
