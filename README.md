EasyRules:
--

EasyRules - GETTING STARTED : https://github.com/j-easy/easy-rules/wiki/getting-started

EasyRules - DEFINING RULES : https://github.com/j-easy/easy-rules/wiki/defining-rules 

EasyRules - COMPOSITE RULES : https://github.com/j-easy/easy-rules/wiki/defining-rules#composite-rules

EasyRules - COMPOSITE RULES : UnitRuleGroup: A unit rule group is a composite rule that acts as a unit: Either all rules are applied or nothing is applied.ActivationRuleGroup: An activation rule group is a composite rule that fires the first applicable rule and ignores other rules in the group (XOR logic). Rules are first sorted by their natural order (priority by default) within the group.ConditionalRuleGroup: A conditional rule group is a composite rule where the rule with the highest priority acts as a condition: if the rule with the highest priority evaluates to true, then the rest of the rules are fired.

EasyRules - DEFINING RULES ENGINE : https://github.com/j-easy/easy-rules/wiki/defining-rules-engine

EasyRules - DEFINING LISTENER : https://github.com/j-easy/easy-rules/wiki/defining-rules-listener

EasyRules - DEFINING RULES ENGINE LISTENER : https://github.com/j-easy/easy-rules/wiki/defining-rules-engine-listener

